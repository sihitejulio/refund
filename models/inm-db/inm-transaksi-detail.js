'use strict';
module.exports = (sequelize, DataTypes) => {
    const TransaksiDetail = sequelize.define('TransaksiDetail', {
        id: {
            type: DataTypes.INTEGER(20).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        transaksi_id: DataTypes.BIGINT(20),
        status_id: DataTypes.TINYINT(4),
        id_pelanggan: DataTypes.STRING(30),
        nama_pelanggan: DataTypes.STRING(100),
        lembar: DataTypes.INTEGER(2),
        jumlah_tagihan: DataTypes.DECIMAL(19,0),
        biaya_admin: DataTypes.DECIMAL(19,0),
        cashback: DataTypes.DECIMAL(19,0),
        total_tagihan: DataTypes.DECIMAL(19,0),
        produk_id: DataTypes.INTEGER(10).UNSIGNED,
        inm_referensi: DataTypes.STRING(50),
        referensi_vendor: DataTypes.STRING(50),
        terbilang: DataTypes.TEXT,
        print_out: DataTypes.TEXT,
        keterangan: DataTypes.STRING(200),
        response_message: DataTypes.TEXT,
        tgl_create: DataTypes.DATE,
        tgl_update: DataTypes.DATE
    }, {
        tableName: 'inm_transaksi_detail',
        timestamps: false,
        underscored: true,
    });

    
  
    return TransaksiDetail;
}
