'use strict';
module.exports = (sequelize, DataTypes) => {
    const Transaksi = sequelize.define('Transaksi', {
        id: {
            type: DataTypes.INTEGER(20).UNSIGNED,
            autoIncrement: true,
			primaryKey: true,
            allowNull: false,
            unique: true
        },
        user_id: DataTypes.INTEGER(20).UNSIGNED,
        kode_transaksi: DataTypes.STRING(100),
        tgl_transaksi: DataTypes.DATE,
        total_bayar: DataTypes.DECIMAL(19,0),
        total_item_bayar: DataTypes.TINYINT(4),

    }, {
            tableName: 'inm_transaksi',
            timestamps: false,
            underscored: true,
        });

    Transaksi.associate = function associate(models) {
        Transaksi.hasMany(models.InmTransaksiDetail, {
            foreignKey: 'transaksi_id',
        });
    };

    // Transaksi.associate = function associate(models) {
    //     Transaksi.hasMany(models.User, {
    //         foreignKey: 'id',
    //         allowNull: false
    //     })
    // }

    return Transaksi;
};
