'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV;
const config = require(__dirname + '/../config/config.json')[env]['inm-db'];
// const logging = require('../../libs/logging');

const db = {};
console.log("DB ENV " + env + " " + JSON.stringify(config))

let sequelize;

sequelize = new Sequelize(config.database, config.username, config.password, Object.assign({}, config, {
  pool:{
    max: 5,
    idle: 30000,
    acquire: 60000,
  },
  operatorsAliases: false,
}));

db.InmTransaksi = sequelize.import('./inm-transaksi');
db.InmTransaksiDetail = sequelize.import('./inm-transaksi-detail');
// db.OAuthClient = sequelize.import('./oauth_client');
// db.UserApi = sequelize.import('./user');

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize.sync().then(result => {
  // logging.info('[service-payment] database api-inm connected ');
  console.log("database connected")
}).catch(err => {
  // logging.error('[service-payment] connection failed...' + err);
});


module.exports = db;
