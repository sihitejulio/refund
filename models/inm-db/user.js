'use strict';
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        nama_user: DataTypes.STRING(30),
        username: {
            type: DataTypes.STRING(20),
            allowNull: false,
            unique: true,
        },
        email:{
            type:DataTypes.STRING(100),
            unique: true
        }, 
        alamat: DataTypes.STRING(100),
        no_telp: DataTypes.STRING(14),
        group_id: DataTypes.STRING(20),
        ca_id: DataTypes.STRING(10),
        level: DataTypes.STRING(10),
        status_id: DataTypes.INTEGER(4),
        tgl_create: DataTypes.DATE,
        tgl_update: DataTypes.DATE,
        info_va_number: DataTypes.TEXT,
        freshchat_id: DataTypes.STRING(100),
        register_at: DataTypes.DATE,
        client_id: DataTypes.STRING(100),
        device_id: DataTypes.STRING(100),
        fcm_code: DataTypes.STRING(100),
        flag: DataTypes.INTEGER(1),
        oauth_token: DataTypes.TEXT,
        session_id:DataTypes.STRING(255),
        gambar: DataTypes.STRING(255)
    }, {
        tableName: 'inm_users', // oauth_users
        timestamps: false,
        underscored: true,
    });
    
    // User.associate =  function associate(models) {
    //     User.hasOne(models.Saldo, {
    //       foreignKey: 'user_id',
    //     });
    // };

    return User;
}
