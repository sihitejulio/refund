const inm_db = require('./models/inm-db')
const Sequelize = require('sequelize');

// var uuid4 = require('uuid4');
let i = 1;

require('fs').readFileSync('file/file.txt', 'utf-8').split(/\r?\n/).forEach(async function (line) {
    let new_line = line.split(" ");
    let harga = new_line[new_line.length - 1].replace(/\./g, '');
    let tanggal = line.match(/\d{4}-[01]\d-[0-3]\d [0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?/gm)[0];
    console.log("Data Ke " + i + " length array " + new_line.length + " tanggal : " + tanggal + " harga : " + harga)
    await inm_db.InmTransaksi.findAll({
        include: [{
            model: inm_db.InmTransaksiDetail,
            where: { total_tagihan: harga }
        }],
        where: { tgl_transaksi: tanggal },
        raw: true
    }).then(function (data) {
        if (data.length > 0) {
            // resolve(data[0].cashback)
            console.log("Data Id : " + data[0]['TransaksiDetails.id'] + " Status Id : " + data[0]['TransaksiDetails.status_id'])
            console.log(" ");
            inm_db.InmTransaksiDetail.update(
                { "status_id": process.env.STATUS },
                {
                    where: { id: data[0]['TransaksiDetails.id'] },
                })
                .then(function (results) {
                    if (results) {
                        console.log("================")
                        console.log("Data Id : " + data[0]['TransaksiDetails.id'])
                        console.log("Update Berhasil")
                    }
                })
                .catch(function (err) {
                    console.log(err)
                })
        }
    })
    i++
});